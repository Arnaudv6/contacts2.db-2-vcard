# export android SQLite contacts database to vCard (VCF)

## what?
Pure Python script to **export contacts as a VCF (vcard)** file format version 2.1-ish,  
using an **Android contacts2.db** sqlite file as input.  

Usage: download script, and run it with python 3. (append option `--help` at need)

Contacts file is often located here on your android smartphone:  
`/data/data/com.android.providers.contacts/databases/contacts2.db`  

## quality concerns
 - todo-list apart, neither pylint, black, flake8 nor bandit tools complains: O-clear.
 - works for me on lineageOS roms
 - prints any contact detail read but not currently exported (physical adresses...): feel free to open a bug for feature request
 - based on previous work from:
     - Andreas Böhler https://www.aboehler.at
     - Ian Worthington http://forum.xda-developers.com/android/help/extract-contacts-backup-t3307684
     - Thomas More https://github.com/tmo1/contacts2vcard

## note worthy alternatives
Should you prefer bash to python, you can turn to either:
 - https://github.com/stachre/dump-contacts2db
 - a more recent edit: https://android.stackexchange.com/a/215426
