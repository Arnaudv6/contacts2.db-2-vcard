#!/usr/bin/env python3

"""
Created by Arnaudv6, https://gitlab.com/Arnaudv6/contacts2.db-2-vcard

based on work from:
    Andreas Böhler
        https://www.aboehler.at
    Ian Worthington
        http://forum.xda-developers.com/android/help/extract-contacts-backup-t3307684
    Thomas More
        https://github.com/tmo1/contacts2vcard
"""
from typing import Any

# todo:
#  - https://docs.python.org/3/library/sqlite3.html#using-the-connection-as-a-context-manager
#  - stop dropping real-estate addresses
#  - utf8 printable encoded names
#  - check usually unused columns for given mimetypes are indeed empty
#  - check all VCF strings in PHONE_TYPES (current values are wild guesses)
#  - add CSV export option?
#  - format telephone numbers according to their countries


import sqlite3
import sys
import re
import argparse
import os

#  Stick to VCARD 2.1, as spec is freely available and simpler.
# Indicate preferred number          PREF
# Indicate a work number             WORK
# Indicate a home number             HOME
# Indicate a voice number            VOICE (Default)
# Indicate a facsimile number        FAX
# Indicate a messaging service       MSG
# Indicate a cellular number         CELL
# Indicate a pager number            PAGER
# Indicate a bulletin board service  BBS
# Indicate a MODEM number            MODEM
# Indicate a car-phone number        CAR
# Indicate an ISDN number            ISDN
# Indicate a video-phone number      VIDEO

PHONE_TYPES = {
    0: ("default", "TEL;PREF:"),
    1: ("home", "TEL;HOME:"),
    2: ("mobile", "TEL;CELL:"),
    3: ("work", "TEL;WORK:"),
    4: ("fax_work", "TEL;FAX;HOME:"),
    5: ("fax_home", "TEL;FAX;WORK:"),
    6: ("pager", "TEL;PAGER:"),
    7: ("other", "TEL;VOICE:"),
    8: ("callback", "TEL;VOICE:"),
    9: ("car", "TEL;CAR:"),
    10: ("company_main", "TEL;WORK:"),
    11: ("isdn", "TEL;ISDN:"),
    12: ("main", "TEL;PREF:"),
    13: ("other_fax", "TEL;FAX:"),
    14: ("radio", "TEL:"),
    15: ("telex", "TEL:"),
    16: ("tty_ttd", "TEL:"),
    17: ("work_mobile", "TEL;WORK;CELL:"),
    18: ("work_pager", "TEL;WORK;PAGER:"),
    19: ("assistant", "TEL:"),
    20: ("mms", "TEL:"),
}


IGNORED_DB_MIMETYPES = [14, 17, 18]  # 14: often google profile, 17 & 18: Signal


class Contact:
    def __init__(self, cid):
        self.cid = cid
        self.name = None
        self.phone_numbers = []
        self.mail_addresses = []
        self.web_addresses = []

    def set_name(self, name):
        # when 2 raw_contact_id, assume the lowest value (first encountered) has
        # better name fields (as they often differ). This proves right for me.
        if not (self.name and self.name.formatted):
            self.name = name

    def get_vcard(self):
        vcard = ['BEGIN:VCARD', 'VERSION:2.1']

        if self.name:
            vcard.append(f"N:{self.name.get_vcard_formatted()}")
            vcard.append(f"FN:{self.name.get_formatted()}")
        else:
            vcard.append("N:;;;;")
            vcard.append("FN:")
            print(f"no name for contact {self.phone_numbers}, {self.mail_addresses}")

        for phone_num in self.phone_numbers:
            try:
                vcard.append(PHONE_TYPES[phone_num[1]][1] + phone_num[0])
            except KeyError:
                print(f"ignoring num {phone_num[0]} with unknown type {phone_num[1]}")
        for mail in self.mail_addresses:
            vcard.append(f"EMAIL;X-INTERNET:{mail}")
            # data_db_email_types = {1:'home', 2:'work', 3:'other', 4:'mobile'}
        for site in self.web_addresses:
            vcard.append(f"URL:{site}")

        vcard.append("END:VCARD")

        return vcard


class Name:
    formatted = ""

    def __init__(self, row):
        self.formatted = row[1] if row[1] else ""
        self.first_name = row[2] if row[2] else ""
        self.family_name = row[3] if row[3] else ""
        self.prefix = row[4] if row[4] else ""  # = title (Dr, Pr...)
        self.middle_name = row[5] if row[5] else ""
        self.suffix = row[6] if row[6] else ""  # (Jr, M.D. ...)

    def get_formatted(self):
        formatted_name = (
            self.formatted
            if self.formatted
            else f"{self.first_name} {self.family_name}".strip()
        )
        return formatted_name

    def get_vcard_formatted(self):
        # each name-field itself could have many parts, separated with comas ','
        return ";".join(
            (
                self.family_name,
                self.first_name,
                self.middle_name,
                self.prefix,
                self.suffix,
            )
        )


class Telephone:
    VALID_REGEX = re.compile(r"^\+[1-9]\d{3,14}$")  # optional '+', 2-13 digits

    def __init__(self, number, tel_type):
        self.number = number  # set then check: we rely on number to print nice errors
        if not number:
            self.is_valid = False
            return
        self.type = tel_type
        self.unformatted = "".join([char for char in number if char not in " -()."])
        self.is_valid = Telephone.VALID_REGEX.match(self.unformatted)

    def get_formatted(self):
        return self.number if self.unformatted != self.number else ""

    def get_non_prefixed(self):
        prefix = 3 if self.unformatted.startswith("+") else 1
        return self.unformatted[prefix:]

    def equals(self, telephone):
        return self.get_non_prefixed() == telephone.get_non_prefixed()


def triage_telephones(contact, row):
    # both data4 and data1? data1 has nice spaces/dashes country-specific format.
    tel1 = Telephone(row[1], row[2])
    tel2 = Telephone(row[4], row[2])

    if tel1.is_valid or tel2.is_valid:
        if tel1.is_valid and tel2.is_valid and not tel1.equals(tel2):
            print(f"Key {row[7]} has 2 numbers: {tel1.number} and {tel2.number}\n")
        phone_formatted = (
            tel1.unformatted if tel1.unformatted.startswith("+") else tel2.unformatted,
            int(row[2]),  # data2: tel type
        )
        if not phone_formatted in contact.phone_numbers:
            contact.phone_numbers.append(phone_formatted)
    else:
        print(f"Key {row[7]}: no valid phone: '{tel1.number}', '{tel2.number}'\n")


def collate_noop(string1, string2):
    del string1, string2
    return 0


def main():
    if sys.version_info < (3, 8):
        sys.exit("Python 3.8+ required.")

    parser = argparse.ArgumentParser(
        add_help=True,
        description="reads sqlite file contacts2.db and outs a vcf (vcard) file.",
        prog="contacts2_db_2_vcard.py",
    )
    parser.add_argument(
        "input",
        type=argparse.FileType("r"),
        help="input (contacts2.db) sqlite file",
    )
    parser.add_argument(
        "output",
        type=argparse.FileType("w"),
        help="output vcard (*.vcf) file",
    )
    arguments = parser.parse_args()

    # no need to expanduser after argparse.FileType()
    sqlite_file = os.path.normpath(arguments.input.name)
    vcard_file = os.path.normpath(arguments.output.name)

    contacts = {}
    # not all contacts from 'data' are listed in 'contacts'. also some contacts have
    # 2 'raw_contact_id' values. special table 'view_data', with 'contact_id' column
    # helps de-duplicate. but depends on 'PHONEBOOK' collation sequence, mocked above.

    connection = sqlite3.Connection(sqlite_file)
    connection.create_collation("PHONEBOOK", collate_noop)
    cursor = connection.cursor()

    # open contacts2.db with sqlitebrowser, and observe:
    #  data1: all mimetypes
    #  data2, data3, data5, data6: useful only when mimetype is 7
    #  data4: mimetype is 5 and 7

    # row         row[0]    row[1] row[2] row[3] row[4] row[5] row[6]    row[7]
    columns = "mimetype_id, data1, data2, data3, data4, data5, data6, contact_id"
    cursor.execute(f"SELECT {columns} FROM view_data ORDER BY 'raw_contact_id' ASC")
    # see comment in mimetype == 7 for reason to sort

    while row := cursor.fetchone():
        try:
            contact = contacts[row[7]]
        except KeyError:
            contacts[row[7]] = Contact(row[7])
            contact = contacts[row[7]]

        mimetype = row[0]
        if mimetype == 1:  # email
            contact.mail_addresses.append(row[1])
        elif mimetype == 5:  # Phone
            triage_telephones(contact, row)
        elif mimetype == 7:  # Name
            contact.set_name(Name(row))
        elif mimetype == 11:  # web
            contact.web_addresses.append(row[1])
        elif mimetype not in IGNORED_DB_MIMETYPES:
            try:
                print(f"Ignored row {row},\n  contact ({contact.name.formatted})\n")
            except AttributeError:
                print(f"Ignored row {row},\n  contact name not fetched yet")

    cursor.close()
    connection.close()

    print(f"read {len(contacts)} contacts, writing *.vcf file")
    with open(vcard_file, "w", encoding="utf8") as out_file:
        for _, contact in contacts.items():
            out_file.write("\n".join(contact.get_vcard()))
            out_file.write("\n\n")


if __name__ == "__main__":
    try:
        main()
    except SystemExit:
        raise  # catching here to avoid catchall bellow
    except (BaseException, Exception, ArithmeticError, BufferError, LookupError):
        # At this point, any uncaught exception is a bug
        print("""\n\n\ncrash: using latest script version? report a bug to arnaudv6:""")
        raise
